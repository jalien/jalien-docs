#!/bin/bash

LOG=`pwd`/out.log
OUT="docs/reference.md"

# These client-specific commands need to be at the top
CLIENT_CMDS=( "cp" "quota" )

# Commands to run as examples
EXAMPLES="
cp,jsh,cp file://example.file example.file
cat,jsh,cat example.file
cd,jsh,cd /alice/cern.ch/user/o/odatskov
chmod,jsh,chmod 400 example.file
du,jsh,du /alice/cern.ch/user/o/odatskov
grep,jsh,grep Hello example.file
groups,jsh,groups
lfn2guid,jsh,lfn2guid example.file
ls,jsh,ls -lFH example.file
md5sum,jsh,md5sum example.file
ping,jsh,ping 5
pwd,jsh,pwd
stat,jsh,stat example.file
toXml,jsh,toXml example.file
type,jsh,type example.file
uptime,jsh,uptime
uuid,jsh,uuid example.file
whereis,jsh,whereis example.file
whoami,jsh,whoami
xrdstat,jsh,xrdstat example.file
"

# File content to use for executing the example
SAMPLE="Hello there! This is just a sample file"
[ ! -f example.file ] && { echo "$SAMPLE" > example.file; }

#################
log() {
#################
  when="[$(date "+%d-%m-%Y %H:%M:%S")]"
  case $1 in
    error)   shift 1; echo -e "\e[31m$when ERROR: $*\e[0m" | tee -a $LOG ; exit 2 ;;
    success) shift 1; echo -e "\e[32m$when DONE: $*\e[0m"  | tee -a $LOG ; exit $FAIL ;; 
    warn)    shift 1; echo -e "\e[33m$when WARN:\e[0m $*"  | tee -a $LOG  ; FAIL=$((FAIL + 1)) ;;
    info)    shift 1; echo -e "\e[34m$when $*\e[0m" | tee -a $LOG ;; 
    silent)  shift 1; echo "$when $*" >> $LOG ;;
    *)       echo -e "$when $*" | tee -a $LOG ;
  esac
}

#################
parse_args() {
#################
  while [ $# -gt 0 ]; do
	  case $1 in
	    --help|-help|-h)
		        echo -e "\nRun AliEn API commands and generate documentation\n\nUsage: $0 <options>\nOptions:\n\t-check, -c\t\tLook for command updates comparing latest and from docs\n\t-run,-r\t\t\tRun latest available commands\n\t-list|-l\t\tList commands specified in the documentation\n\t-force|-f\t\tForce regeneration of command documentation\n\t-out|-o <filename>\tOutput markdown file name. Default: $OUT\n"; exit 0;;
	    -check|-c)  get_cmds ; log success "Finished checking commands";;
	    -list|-l)   [ ! -f $OUT ] && { log error "Docs not found: $OUT. Generate docs first. Exiting..."; }
			dlist=( `cat $OUT | grep '## **' |  cut -d'"' -f2` ); log "Docs contain ${#dlist[@]} command(s): ${dlist[@]}"; exit 0;;
	    -force|-f)  RUN=1 ;;
	    -out|-o)    shift 1 ; OUT=$1 ;;
	    *) log error "Invalid argument: $*. Please use: $0 -help for the list of available options."
	  esac
	  shift 1
  done
}

#################
get_cmds() {
#################

  # Get fresh lists of commands from jalien and alien.py
  jalien -e commandlist > jalien.list
  [ ! $? -eq 0 ] && { log error "Failed to retrieve commands: jalien -e commandlist"; }

  alien.py commandlist > alienpy.list
  [ ! $? -eq 0 ] && { log error "Failed to retrieve commands: alien.py commandlist"; }

  # Ensure that the two lists match before proceeding
  DIFF=`diff -u jalien.list alienpy.list`
  [ ! "$DIFF" == "" ] && { log error "JAliEn and alien.py command lists do not match: $DIFF"; }

  list=( `cat jalien.list` )
  log info "Latest: ${#list[@]} command(s)"

  # For existing documentation we compare commands with the latest ones
  if [ -f $OUT ]; then 
	olist=( `cat $OUT | grep '## **' |  cut -d'*' -f3` ); toadd=""
	log info "Documentation: ${#olist[@]} command(s)"; 

        # Find missing and removed commands
        for cmd in "${list[@]}"; do
		[[ ${olist[@]} =~ (^| )$cmd($| ) ]] && olist=( `echo ${olist[@]} | sed -e "s/ *$cmd */ /"` ) || toadd=( "${toadd[@]}" "$cmd" )
	done

	toadd=( `echo "${toadd[@]}" | xargs` )

	[ ${#toadd[@]} -gt 0 ] && { log "List of ${#toadd[@]} command(s) to be added: ${toadd[@]}"; RUN=1; }
        [ ${#olist[@]} -gt 0 ] && { log "List of ${#olist[@]} command(s) to be removed: ${olist[@]}"; RUN=1; }
	[[ (${#toadd[@]} -eq 0) && (${#olist[@]} -eq 0) ]] && { log "All ${#list[@]} commands match."; }
  else  log "Documentation not found: $OUT. Nothing to compare."; RUN=1; fi
  rm -f jalien.list alienpy.list
}

#################
run_example() {
#################

	excmds=`echo "$EXAMPLES" | grep "^$1,"`

	if [ ! -z "$excmds" ]; then
		
		client=$(echo $excmds | cut -d',' -f2)
		excmd=$(echo $excmds | cut -d',' -f3)
		log info "Running $client example: $excmd"

		if [ "$client" == "jsh" ]; then 	EXEC=$(jalien -e "$excmd");
		elif [ "$client" == "alien.py" ]; then 	EXEC=$(alien.py "$excmd"); fi 
		[ ! $? -eq 0 ] && { log warn "Failed to run example: $client $excmd"; }

		TYPE="Example"
		[ "$client" == "jsh" ] && { EXCMD="$client: [alice]"; } || { EXCMD="JAliEn[username]:"; }
		help_doc "$(echo -e "$EXCMD > $excmd\n$EXEC" | sed "s%/$(whoami | head -c1)/$(whoami)%/u/username%g; s%$(whoami)%username%g;")"
	fi
}

#################
help_doc() { echo -e "=== \"$TYPE\"\n    \`\`\`console\n    $(echo -e "$@" | sed ':a;N;$!ba;s/\n/\n    /g')\n    \`\`\`\n" >> $OUT.tmp; }
#################

#################
cmd_docs() {
#################

	log info "Looking at $1"

	# Adding the command title
  	echo -e "## **$1**\n" >> $OUT.tmp

	[ $CLIENT -eq 1 ] && { echo -e "Options for this command are client-specific. Refer to the relevant client usage as shown below.\n" >> $OUT.tmp; }

	# Retrieve command help using both jalien and alien.py
	HELP=$(jalien -e "$1 -h" )
        [ ! $? -eq 0 ] && log warn "Failed to execute: jalien -e $cmd -h"

	HELPY=$(alien.py "$1 -h")
	[ ! $? -eq 0 ] && log warn "Failed to execute: alien.py $cmd -h"

	# When both command option match and are not empty we write generic usage
	if   [[ -z "$HELP" && -z "$HELPY" ]]; then 
		echo -e "No help available for this command\n" >> $OUT.tmp;

	elif [[ "$(diff <(echo $HELP | sed '/^[[:space:]]*$/d') <(echo $HELPY | sed '/^[[:space:]]*$/d'))" == "" ]]; then 
		TYPE="Usage"; help_doc "$HELP"; 

	else
		[ ! -z "$HELP" ] &&  { TYPE="Usage: jalien" help_doc "$HELP"; }
		[ ! -z "$HELPY" ] && { TYPE="Usage: alien.py" help_doc "$HELPY"; }; fi

	# Run examples when available
	run_example "$1"
}

#################
### Main ########

[ -f $LOG ] && { rm $LOG; log silent "Cleaned previous log file: $LOG"; }
[ -f $OUT.tmp ] && rm $OUT.tmp;

which jalien 2>&1 >/dev/null
[ ! $? -eq 0 ] && log error "Failed to find jalien. Please ensure environment is setup. Exiting..."

which alien.py 2>&1 >/dev/null
[ ! $? -eq 0 ] && log error "Failed to find alien.py. Please ensure environment is setup. Exiting..."

# Parse arguments and get latest commands
RUN=0; FAIL=0; parse_args $*; get_cmds

if [ $RUN -eq 1 ]; then
	log "Starting docs generation"
	echo -e "# Command Reference\n" >> $OUT.tmp

	# Generate docs for client commands at the top
        for cmd in ${CLIENT_CMDS[@]}; do CLIENT=1 cmd_docs "$cmd"; done

	# Then add the rest of the commands
	for cmd in ${list[@]}; do
		[[ ${CLIENT_CMDS[@]} =~ "$cmd" ]] && { continue; } || { CLIENT=0 cmd_docs "$cmd"; }
	done
	python alienpy_help_mkdoc.py > alienpy_commands.md
	bash jalien_help_mkdoc.sh
	mv $OUT.tmp $OUT ; rm -f example.file; log success "Finished generating docs"
else    log "Nothing to generate. Skipping..."; exit 0; fi
