#!/bin/bash

LOG=`pwd`/out.log
OUT="docs/jalien_commands.md"

# These client-specific commands need to be at the top
CLIENT_CMDS=( "cp" "quota" )

#################
log() {
#################
  when="[$(date "+%d-%m-%Y %H:%M:%S")]"
  case $1 in
    error)   shift 1; echo -e "\e[31m$when ERROR: $*\e[0m" | tee -a $LOG ; exit 2 ;;
    success) shift 1; echo -e "\e[32m$when DONE: $*\e[0m"  | tee -a $LOG ; exit $FAIL ;; 
    warn)    shift 1; echo -e "\e[33m$when WARN:\e[0m $*"  | tee -a $LOG  ; FAIL=$((FAIL + 1)) ;;
    info)    shift 1; echo -e "\e[34m$when $*\e[0m" | tee -a $LOG ;; 
    silent)  shift 1; echo "$when $*" >> $LOG ;;
    *)       echo -e "$when $*" | tee -a $LOG ;
  esac
}

#################
parse_args() {
#################
  while [ $# -gt 0 ]; do
	  case $1 in
	    --help|-help|-h)
		        echo -e "\nRun AliEn API commands and generate documentation\n\nUsage: $0 <options>\nOptions:\n\t-check, -c\t\tLook for command updates comparing latest and from docs\n\t-run,-r\t\t\tRun latest available commands\n\t-list|-l\t\tList commands specified in the documentation\n\t-force|-f\t\tForce regeneration of command documentation\n\t-out|-o <filename>\tOutput markdown file name. Default: $OUT\n"; exit 0;;
	    -check|-c)  get_cmds ; log success "Finished checking commands";;
	    -list|-l)   [ ! -f $OUT ] && { log error "Docs not found: $OUT. Generate docs first. Exiting..."; }
			dlist=( `cat $OUT | grep '## **' |  cut -d'"' -f2` ); log "Docs contain ${#dlist[@]} command(s): ${dlist[@]}"; exit 0;;
	    -force|-f)  RUN=1 ;;
	    -out|-o)    shift 1 ; OUT=$1 ;;
	    *) log error "Invalid argument: $*. Please use: $0 -help for the list of available options."
	  esac
	  shift 1
  done
}

#################
get_cmds() {
#################

  # Get fresh lists of commands from jalien
  jalien -e commandlist > jalien.list
  [ ! $? -eq 0 ] && { log error "Failed to retrieve commands: jalien -e commandlist"; }

  list=( `cat jalien.list` )
  log info "Latest: ${#list[@]} command(s)"

  # For existing documentation we compare commands with the latest ones
  if [ -f $OUT ]; then 
	olist=( `cat $OUT | grep '###' |  cut -d'#' -f4` ); toadd=""
	log info "Documentation: ${#olist[@]} command(s)"; 

        # Find missing and removed commands
        for cmd in "${list[@]}"; do
		[[ ${olist[@]} =~ (^| )$cmd($| ) ]] && olist=( `echo ${olist[@]} | sed -e "s/ *$cmd */ /"` ) || toadd=( "${toadd[@]}" "$cmd" )
	done

	toadd=( `echo "${toadd[@]}" | xargs` )

	[ ${#toadd[@]} -gt 0 ] && { log "List of ${#toadd[@]} command(s) to be added: ${toadd[@]}"; RUN=1; }
        [ ${#olist[@]} -gt 0 ] && { log "List of ${#olist[@]} command(s) to be removed: ${olist[@]}"; RUN=1; }
	[[ (${#toadd[@]} -eq 0) && (${#olist[@]} -eq 0) ]] && { log "All ${#list[@]} commands match."; }
  else  log "Documentation not found: $OUT. Nothing to compare."; RUN=1; fi
  rm -f jalien.list
}

#################
help_doc() { echo -e "\`\`\` $(echo -e "$@" | sed ':a;N;$!ba;s/\n/\n    /g')\n\`\`\`\n" >> $OUT.tmp; }
#################

#################
cmd_docs() {
#################

        log info "Looking at $1"

        # Adding the command title
        echo -e "### $1" >> $OUT.tmp

        # Retrieve command help using jalien
        HELP=$(jalien -e "$1 -h" )
        [ ! $? -eq 0 ] && log warn "Failed to execute: jalien -e $cmd -h"

        [ ! -z "$HELP" ] &&  { TYPE= help_doc "$HELP"; }

        [ $CLIENT -eq 1 ] && { echo -e "!!! warning \"client-side implementation\"\n" >> $OUT.tmp; }
}

#################
### Main ########

[ -f $LOG ] && { rm $LOG; log silent "Cleaned previous log file: $LOG"; }
[ -f $OUT.tmp ] && rm $OUT.tmp;

which jalien 2>&1 >/dev/null
[ ! $? -eq 0 ] && log error "Failed to find jalien. Please ensure environment is setup. Exiting..."

# Parse arguments and get latest commands
RUN=0; FAIL=0; parse_args $*; get_cmds

if [ $RUN -eq 1 ]; then
        log "Starting docs generation"
        echo -e "# jalien Command Reference\n" >> $OUT.tmp

        # Then add the rest of the commands
        for cmd in ${list[@]}; do
                [[ ${CLIENT_CMDS[@]} =~ "$cmd" ]] && { CLIENT=1 cmd_docs "$cmd"; } || { CLIENT=0 cmd_docs "$cmd"; }
                echo -e "---\n" >> $OUT.tmp
        done
        mv $OUT.tmp $OUT ; rm -f example.file; log success "Finished generating docs"
else    log "Nothing to generate. Skipping..."; exit 0; fi

