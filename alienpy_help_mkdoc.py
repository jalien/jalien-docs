#!/usr/bin/env python3

import shutil
import os
import sys

# import alienpy functions
try:
    from alienpy.wb_api import PrintDict, retf_print
    from alienpy.alien import *  # nosec PYL-W0614
except Exception:
    try:
        from xjalienfs.wb_api import PrintDict, retf_print
        from xjalienfs.alien import *  # nosec PYL-W0614
    except Exception:
        print("Can't load alienpy, exiting...")
        sys.exit(1)

# enable automatic pretty printing
from rich import print

########################################
##   REQUIRED INITIAILZATION

# Enable and setup logging
setup_logging()  # type: ignore

# Create connection to JAliEn services
wb = InitConnection(cmdlist_func = constructCmdList)  # type: ignore

##   END OF INITIALIZATION
########################################

f_client_nowb = set(AlienSessionInfo['cmd2func_map_nowb'])
f_client_wb = set(AlienSessionInfo['cmd2func_map_client'])
f_server_wb = set(AlienSessionInfo['cmd2func_map_srv'])

def help2markup(cmd: str):
    if not cmd: return
    result = None
    if cmd in AlienSessionInfo['cmd2func_map_nowb']:
        result = AlienSessionInfo['cmd2func_map_nowb'][cmd](['-h'])
    elif cmd in AlienSessionInfo['cmd2func_map_client']:
        result = AlienSessionInfo['cmd2func_map_client'][cmd](wb, ['-h'])
    elif cmd in AlienSessionInfo['cmd2func_map_srv']:
        result = AlienSessionInfo['cmd2func_map_srv'][cmd](wb, cmd, ['-h'], 'nokeys')

    if not result or not result.out:
        print_err(result)
        print_err(f'FAILED!! : {cmd}')
        return

    help_txt = result.out
    help_txt = help_txt.replace('                ', '')
    print_out(f'\n### {cmd}\n```\n{help_txt}\n```\n')
    if cmd in AlienSessionInfo['cmd2func_map_client']:
        print_out('!!! warning "client-side implementation or improvement of the central servers version"\n')
    if cmd in AlienSessionInfo['cmd2func_map_nowb']:
        print_out('!!! warning "client-side implementation"\n\n!!! note "No connection to central servers needed"\n')
    print_out('---')


print_out('# alien.py Command reference guide')
for cmd in AlienSessionInfo['commandlist']: help2markup(cmd)
print()

