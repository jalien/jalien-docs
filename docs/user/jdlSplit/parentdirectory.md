### **Parentdirectory split strategy**

Seperates inputdata files into different subjobs based on full path to the parent of the lowest directory in LFN path 
and file size or number of files restrictions.

Example: /alice/cern.ch/user/j/jalien/inputdatafile --> /alice/cern.ch/user/j

Example JDL:
``` 
User = "jalien";
JobTag = {
"Parentdirectory split!"
};
Packages = {
"VO_ALICE@O2Physics::daily-20241202-0000-1"
};
Executable = "/alice/cern.ch/user/j/jalien/bin/splitting.sh";
InputDataCollection = "LF:/alice/cern.ch/user/j/jalien/inputdatafileCollection.xml,nodownload";
Split = "parentdirectory";
```
Optional flags in JDL:  
[SplitMaxInputFileNumber](../../../jdl_syntax#splitmaxinputfilenumber)  
[SplitMaxInputFileSize](../../../jdl_syntax#splitmaxinputfilesize)  
[OrderLFN](../../../jdl_syntax#orderlfn)  
