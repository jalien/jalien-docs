# VOBox

This document describes how to install and configure the site VO-Box to support ALICE VO services.
This is a node on which long-lived agents and services will be deployed.
These are expected to be provided at the sites. 
The agents/services deployment and support on the VO-Box is under VO responsibility.

See the following quick links to setup steps depending on your preferred deployment approach:
    
| | |
|-|-|
| __Generic/VM__ | Step 1: [General requirements](#requirements), [Network setup](#requirements)<br>Step 2: [WLCG VO-Box Installation](#wlcg-vo-box)<br>Step 3: [HTCondor/ARC Specifics](../vobox_htc_arc/)<br>Step 4: [Grid Monitoring: MonALISA](../monalisa/#deployment-and-configuration) |
| __Container__ | Step 1: [Container requirements](../vobox_container/#requirements), [Network Setup](../vobox_container/#setup-networking)<br>Step 2: [Install HTCondor/ARC VOBox container](../vobox_container/#create-container)<br>Step 3: [Grid Monitoring: MonALISA](../monalisa/#deployment-and-configuration) |

## Requirements

General requirements for the VO node agents/services are as follows:

| | |
|-|-|
| __OS__ | RH/Alma/Rocky EL9, 64-bit Linux. The machine usually will need to be a WLCG VOBOX |
| __Hardware__ | Minimum 4GB RAM, any standard CPU, 20GB for logs, 5GB cache |

## Network

The following network connectivity is expected for the VO-Box services:

| Port | Access | Service |
|:----:|:-------|:--------|
| 1093 | TCP Incoming from the World | MonALISA FDT server, SE tests |
| 8884 | *UDP* Incoming from your __site WN__ and your __site SE nodes__ | Monitoring info |
| 9930 | *UDP* Incoming from your __site SE nodes__ | XRootD metrics |
|      | ICMP Incoming and Outgoing | Network topology for file placement and access |

In the future, these extra services __may__ be needed:

| Port | Access | Service |
|:----:|:-------|:--------|
| 8098 | TCP Incoming from your __site WN__ | JAliEn/Java Serialized Object stream |
| 8097 | TCP Incoming from your __site WN__ | JAliEn/WebSocketS |

!!! note ""
    In general, the assumption is that the __outgoing__ connectivity from the VO-box and the WNs is __unrestricted__.

__CERN__ has multiple networks that may all be used for Central Services, already now or in the future:

| Protocol | IP Range | Note |
|:--------:|:---------|:-----|
| IPv4 | __128.141.0.0/16__ | |
|      | __128.142.0.0/16__ | |
|      | __128.142.249.0/24__ | <- part of Central Services are here |
|      | __137.138.0.0/16__ | |
|      | __188.184.0.0/15__ | |
|      | __185.249.56.0/22__ | |
|      | __192.65.196.0/23__ | |
|      | __192.91.242.0/24__ | | 
|      | __194.12.128.0/18__ | |
| IPv6 | __2001:1458::/32__ | |
|      | __2001:1458:301:54::/64__ | <- part of Central Services are here | 
|      | __2001:1459::/32__ | | 

!!! hint
    Please mind the address masks in the above table

## WLCG VO-Box

The VO-Box usually should be preinstalled as a standard __WLCG VO-Box__, following the instructions given at:<br><br>
<https://twiki.cern.ch/twiki/bin/view/LCG/WLCGvoboxDeployment>

This procedure sets up a standard gLite UI, with the following additions (in particular provided by ```lcg-vobox``` RPM):

* Only one local user account __alicesgm__ (or equivalent), with no special privileges. Please DO NOT configure pool accounts for the SGM user on the VO-Box!
* Access via gsissh, with selected users from the ALICE LCG VO mapped to the __alicesgm__ account (YAIM handles that)
* A proxy renewal service running, for the automatic renewal or registered proxies via the MyProxy mechanism (```idem```)
* A host certificate, issued by one of the trusted LCG Certification Authorities. The machine also needs to be registered as a trusted host in the CERN MyProxy server, ```myproxy.cern.ch```.

!!! hint "MyProxy"
    To have the machine registered as trusted host in myproxy.cern.ch, send an email with the host certificate DN to <Maarten.Litmaath@cern.ch>. You can get the host certificate DN by issuing the following command:
    ```console
    VO-Box> openssl x509 -in /etc/grid-security/hostcert.pem -noout -subject
    ```

Additionally, specifically for ALICE, the following configuration details are required:

* The home directory should not be mounted via NFS from some server (for performance reasons and because lock files may be kept there)
* The experiment software is provided on the VO-box and Worker nodes through CVMFS. See the 'Setup CVMFS' section.

